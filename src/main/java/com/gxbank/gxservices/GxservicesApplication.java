package com.gxbank.gxservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GxservicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(GxservicesApplication.class, args);
	}
}
